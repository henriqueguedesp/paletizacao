/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paletizacaov10;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.Random;
import javax.swing.JFrame;
import javax.swing.JPanel;


/**
 *
 * @author Adriane
 */
public class Desenho extends JPanel {

    JFrame frame;

    Random rd = new Random();

    Caixa individuoOtimo[];
    int paleteComprimento, paleteLargura, caixaComp, caixaLarg;

    public Desenho(Caixa individuoOtimo[], int paleteComprimento, int paleteLargura, int caixaComp, int caixaLarg) {

        this.individuoOtimo = individuoOtimo;
        this.paleteComprimento = paleteComprimento;
        this.paleteLargura = paleteLargura;
        this.caixaComp = caixaComp;
        this.caixaLarg = caixaLarg;

        frame = new JFrame();
        frame.setVisible(true);
        frame.setTitle(" ::: PALETIZAÇÃO ::: ");
        frame.setSize(paleteComprimento * 70, paleteLargura * 70);
        frame.setBackground(Color.white);
        frame.add(this);
    }

    public void paintComponent(Graphics g) {

        Graphics2D g2d = (Graphics2D) g;

        for (int pop = 0; pop < individuoOtimo.length; pop++) {
            g2d.setColor(new Color(rd.nextInt(250), rd.nextInt(250), rd.nextInt(250)));

            if (individuoOtimo[pop] != null) {
                if (individuoOtimo[pop].getOrientacao() == 0) {
                    // DESENHA UM RETÂNGULO PREENCHIDO. FILLRECT(X,Y,COMPRIMENTO, LARGURA)
                    g2d.fillRect(individuoOtimo[pop].getX() * 70, individuoOtimo[pop].getY() * 70, caixaLarg * 70, caixaComp * 70);
                } else {
                    g2d.fillRect(individuoOtimo[pop].getX() * 70, individuoOtimo[pop].getY() * 70, caixaComp * 70, caixaLarg * 70);
                }
            }
        }

    }

}
