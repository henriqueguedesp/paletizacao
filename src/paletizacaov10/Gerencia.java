/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paletizacaov10;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author dominador
 */
public class Gerencia {

    private int caixaComp;
    private int caixaLarg;
    private int areaCaixa;
    private int paleteComprimento;
    private int paleteLargura;
    private int areaPalete;
    private int qntGenes; // Pra representar meu individuo (Quantas caixas)
    private int tamPopulacao; // Quantos individuos terá
    private int geracoes;
    private int ger = 0;
    private int qtdAptdaoMaxima = 0;
    private String o;

    private int geracaoOtima;
    private int mutacao; // % de chance de acontecer
    private int x, y, orientacao;
    private Caixa caixa;
    private int mediaAptidoes[];

    Caixa populacao[][] = new Caixa[tamPopulacao][qntGenes];
    Caixa novaPopulacao[][] = new Caixa[tamPopulacao][qntGenes];
    Caixa individuoOtimo[] = new Caixa[qntGenes];
    Caixa pai1[];
    Caixa pai2[];
    Caixa filho1[];
    Caixa filho2[];
    Random radom = new Random();

    public void receberDados() {
        Scanner ler = new Scanner(System.in);

        System.out.println(" Digite o comprimento do Palete: ");
        paleteComprimento = ler.nextInt();

        System.out.println(" Digite a largura do Palete: ");
        paleteLargura = ler.nextInt();

        System.out.println(" Digite o comprimento da caixa: ");
        caixaComp = ler.nextInt();
        while (caixaComp > paleteComprimento) {
            System.out.println(" O comprimento da caixa tem que ser menor que o comprimento do palete: ");
            caixaComp = ler.nextInt();
        }

        System.out.println(" Digite a largura da caixa: ");
        caixaLarg = ler.nextInt();
        while (caixaLarg > paleteLargura) {
            System.out.println(" A largura da caixa tem que ser menor que a largura do palete: ");
            caixaLarg = ler.nextInt();
        }

        System.out.println("");
        areaCaixa = caixaComp * caixaLarg;
        areaPalete = paleteComprimento * paleteLargura;
        qntGenes = areaPalete / areaCaixa;
        System.out.println(" Quantidade máxima de caixas no Palete: " + qntGenes);
        System.out.println("");

        System.out.println(" INFORME OS DADOS ABAIXO ");
        System.out.println(" Tamanho da População: ");
        tamPopulacao = ler.nextInt();
        System.out.println(" Gerações: ");
        geracoes = ler.nextInt();
        System.out.println(" Mutação: ");
        mutacao = ler.nextInt();

        mediaAptidoes = new int[geracoes + 1];
        populacao = new Caixa[tamPopulacao][qntGenes];
        novaPopulacao = new Caixa[tamPopulacao][qntGenes];
        individuoOtimo = new Caixa[qntGenes];

        gerarPopulacaoInicial();

    }

    public void gerarPopulacaoInicial() {  // Gera de forma aleatória minha população inicial
        for (int pop = 0; pop < tamPopulacao; pop++) {
            for (int gene = 0; gene < qntGenes; gene++) {

                gerarCaixaAleatoria();

                if (caixaDentroDoPalete(caixa) == true) {
                    boolean res = verificarSobreposicaoCaixa(caixa, pop);
                    if (res == true) { // Se ao verificar, nenhuma caixa sobrepor outra, então posso coloca-la na minha linha individuo
                        populacao[pop][gene] = caixa;
                    }
                }
            }
        }

        mediaAptidoes[0] = mediaAptidaoPopulacao(populacao);

        System.out.println("");
        System.out.println(" ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ");
        System.out.println(" +                                                                             GERAÇÃO DE POPULAÇÃO: 0                                                                             + ");
        System.out.println(" ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ");
        System.out.println("");
        mostrarPopulacao();
        buscarSolucaoOtima(0);
        cruzamentoPais();
    }

    public void gerarCaixaAleatoria() {
        orientacao = radom.nextInt(2);
        if (orientacao == 0) { // Para não deixar as caixas fora do Palete
            if (paleteComprimento - caixaComp != 0) {
                x = radom.nextInt((paleteComprimento - caixaComp) + 1);
            }
            if (paleteLargura - caixaLarg != 0) {
                y = radom.nextInt((paleteLargura - caixaLarg) + 1);
            }
        } else {
            if (paleteLargura - caixaLarg != 0) {
                x = radom.nextInt((paleteLargura - caixaLarg) + 1);
            }

            if (paleteComprimento - caixaComp != 0) {
                y = radom.nextInt((paleteComprimento - caixaComp) + 1);
            }
        }
        caixa = new Caixa(x, y, orientacao);
    }

    public boolean verificarSobreposicaoCaixa(Caixa c, int pop) {
        for (int gene = 0; gene < qntGenes; gene++) {
            if (populacao[pop][gene] != null) {
                if (c.getOrientacao() == 0) {
                    if (populacao[pop][gene].getOrientacao() == 0) {
                        if (((c.getX() + caixaComp <= populacao[pop][gene].getX()) || (c.getX() >= populacao[pop][gene].getX() + caixaComp))
                                || ((c.getY() + caixaLarg <= populacao[pop][gene].getY()) || (c.getY() >= populacao[pop][gene].getY() + caixaLarg))) {
                        } else {
                            return false;
                        }
                    } else {
                        if (((c.getX() + caixaComp <= populacao[pop][gene].getX()) || (c.getX() >= populacao[pop][gene].getX() + caixaLarg))
                                || ((c.getY() + caixaLarg <= populacao[pop][gene].getY()) || (c.getY() >= populacao[pop][gene].getY() + caixaComp))) {
                        } else {
                            return false;
                        }
                    }

                } else {
                    if (populacao[pop][gene].getOrientacao() == 0) {
                        if (((c.getX() + caixaLarg <= populacao[pop][gene].getX()) || (c.getX() >= populacao[pop][gene].getX() + caixaComp))
                                || ((c.getY() + caixaComp <= populacao[pop][gene].getY()) || (c.getY() >= populacao[pop][gene].getY() + caixaLarg))) {

                        } else {
                            return false;
                        }
                    } else {
                        if (((c.getX() + caixaLarg <= populacao[pop][gene].getX()) || (c.getX() >= populacao[pop][gene].getX() + caixaLarg))
                                || ((c.getY() + caixaComp <= populacao[pop][gene].getY()) || (c.getY() >= populacao[pop][gene].getY() + caixaComp))) {
                        } else {
                            return false;
                        }

                    }
                }
            }
        }
        return true;
    }

    public boolean caixaDentroDoPalete(Caixa c) {
        if (c != null) {
            if (c.getOrientacao() == 0) {
                if (c.getX() <= paleteComprimento - caixaComp) { // Verificar se a caixa não estará fora do palete
                } else {
                    return false;
                }

                if (c.getY() <= paleteLargura - caixaLarg) {
                } else {
                    return false;
                }
            } else {
                if (c.getX() <= paleteLargura - caixaLarg) { // Verificar se a caixa não estará fora do palete
                } else {
                    return false;
                }

                if (c.getY() <= paleteComprimento - caixaComp) {
                } else {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean validaIndividuo(Caixa filho[], Caixa c, int posicao) {
        for (int gene = 0; gene < qntGenes; gene++) {  // fazer separado
            if ((filho[gene] != null) && (c != null) && (gene != posicao)) {
                if (c.getOrientacao() == 0) {
                    if (filho[gene].getOrientacao() == 0) {
                        if (((c.getX() + caixaComp <= filho[gene].getX()) || (c.getX() >= filho[gene].getX() + caixaComp))
                                || ((c.getY() + caixaLarg <= filho[gene].getY()) || (c.getY() >= filho[gene].getY() + caixaLarg))) {
                        } else {
                            return false;
                        }
                    } else {
                        if (((c.getX() + caixaComp <= filho[gene].getX()) || (c.getX() >= filho[gene].getX() + caixaLarg))
                                || ((c.getY() + caixaLarg <= filho[gene].getY()) || (c.getY() >= filho[gene].getY() + caixaComp))) {
                        } else {
                            return false;
                        }
                    }

                } else {
                    if (filho[gene].getOrientacao() == 0) {
                        if (((c.getX() + caixaLarg <= filho[gene].getX()) || (c.getX() >= filho[gene].getX() + caixaComp))
                                || ((c.getY() + caixaComp <= filho[gene].getY()) || (c.getY() >= filho[gene].getY() + caixaLarg))) {

                        } else {
                            return false;
                        }
                    } else {
                        if (((c.getX() + caixaLarg <= filho[gene].getX()) || (c.getX() >= filho[gene].getX() + caixaLarg))
                                || ((c.getY() + caixaComp <= filho[gene].getY()) || (c.getY() >= filho[gene].getY() + caixaComp))) {
                        } else {
                            return false;
                        }

                    }
                }
            }
        }
        return true;
    }

    public void buscarSolucaoOtima(int geracao) {
        int qntCaixasIndividuo = 0;
        for (int ind = 0; ind < tamPopulacao; ind++) {
            for (int gene = 0; gene < qntGenes; gene++) {
                if (populacao[ind][gene] != null) {
                    qntCaixasIndividuo++;
                }
            }

            if (qntCaixasIndividuo > avaliarAptdaoIndividuoVetor(individuoOtimo)) { // Busco a solução òtima e guardo 'em individuoOtimo[]'
                geracaoOtima = geracao;
                individuoOtimo = new Caixa[qntGenes];
                for (int gene = 0; gene < qntGenes; gene++) {
                    individuoOtimo[gene] = populacao[ind][gene];
                }
            }
            qntCaixasIndividuo = 0;
        }
        mostrarSolucaoOtimo();
    }

    public int mediaAptidaoPopulacao(Caixa populacao[][]) { // Avalio a aptidao da população toda e também de cada individuo
        int qntCaixasTotal = 0;
        int mediaAptidao;

        for (int ind = 0; ind < tamPopulacao; ind++) {
            for (int gene = 0; gene < qntGenes; gene++) {
                if (populacao[ind][gene] != null) {
                    qntCaixasTotal++;
                }
            }
        }

        mediaAptidao = qntCaixasTotal / tamPopulacao;
        return mediaAptidao;
    }

    public int avaliarAptdaoIndividuo(int pop) {  // Avalia a aptidão de 'um individuo' em particular pela sua posição
        int qntCaixas = 0;
        for (int gene = 0; gene < qntGenes; gene++) {
            if (populacao[pop][gene] != null) {
                qntCaixas++;
            }
        }
        return qntCaixas;
    }

    public int avaliarAptdaoIndividuoVetor(Caixa vet[]) {  // Avalia a aptidão de 'um individuo' armazenado no vetor
        int qntCaixas = 0;
        if (vet != null) {
            for (int gene = 0; gene < qntGenes; gene++) {
                if (vet[gene] != null) {
                    qntCaixas++;
                }
            }
        }
        return qntCaixas;
    }

    public void torneio() {  // Preenche os dois vetores de 'Pai1' e 'Pai2'
        System.out.println("");
        System.out.println(" SELEÇÃO DE DOIS NOVOS PAIS PARA O TORNEIO ");
        System.out.println("");
        pai1 = new Caixa[qntGenes];
        pai2 = new Caixa[qntGenes];

        for (int torn = 0; torn < 2; torn++) {
            int p1 = radom.nextInt(tamPopulacao);
            int p2 = radom.nextInt(tamPopulacao);

            if (torn == 0) {
                if (avaliarAptdaoIndividuo(p1) > avaliarAptdaoIndividuo(p2)) {
                    System.out.println(" Posicao do Pai1 selecionado: " + p1);
                    for (int gene = 0; gene < qntGenes; gene++) {
                        if (populacao[p1][gene] == null) {
                            pai1[gene] = null;
                        } else {
                            pai1[gene] = new Caixa(populacao[p1][gene].getX(), populacao[p1][gene].getY(),
                                    populacao[p1][gene].getOrientacao());
                        }
                    }
                } else {
                    System.out.println(" Posicao do Pai1 selecionado: " + p2);
                    for (int gene = 0; gene < qntGenes; gene++) {
                        if (populacao[p2][gene] == null) {
                            pai1[gene] = null;
                        } else {
                            pai1[gene] = new Caixa(populacao[p2][gene].getX(), populacao[p2][gene].getY(),
                                    populacao[p2][gene].getOrientacao());
                        }
                    }
                }
            } else {
                if (avaliarAptdaoIndividuo(p1) > avaliarAptdaoIndividuo(p2)) {
                    System.out.println(" Posicao do Pai2 selecionado: " + p1);
                    for (int gene = 0; gene < qntGenes; gene++) {
                        if (populacao[p1][gene] == null) {
                            pai2[gene] = null;
                        } else {
                            pai2[gene] = new Caixa(populacao[p1][gene].getX(), populacao[p1][gene].getY(),
                                    populacao[p1][gene].getOrientacao());
                        }
                    }
                } else {
                    System.out.println(" Posicao do Pai2 selecionado: " + p2);
                    for (int gene = 0; gene < qntGenes; gene++) {
                        if (populacao[p2][gene] == null) {
                            pai2[gene] = null;
                        } else {
                            pai2[gene] = new Caixa(populacao[p2][gene].getX(), populacao[p2][gene].getY(),
                                    populacao[p2][gene].getOrientacao());
                        }
                    }
                }
            }
        }
    }

    public void cruzamentoPais() { // Pego os dois vetores 'Pais' e gero os Dois vetores 'Filhos'

        while (qtdAptdaoMaxima < qntGenes) {
            ger++;
            int ind = 0;
            int mutacaoFilho, mutacaoGene;

            if (ger != 0) {

                mediaAptidoes[ger] = mediaAptidaoPopulacao(populacao);

                System.out.println("");
                System.out.println(" ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ");
                System.out.println(" +                                                                             GERAÇÃO DE POPULAÇÃO: " + ger + "                                                                             + ");
                System.out.println(" ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ");
                System.out.println("");
                mostrarPopulacao();
                System.out.println("");

                buscarSolucaoOtima(ger);
            }

            while (ind < tamPopulacao) { // Uso while pois se meus dois filhos forem válidos, vou inserir 2 individuos de uma vez na minha nova população
                torneio();
                int corte;
                filho1 = new Caixa[qntGenes];
                filho2 = new Caixa[qntGenes];
                corte = radom.nextInt(qntGenes - 1);

                for (int cont = 0; cont <= corte; cont++) {
                    filho1[cont] = pai1[cont];
                    filho2[cont] = pai2[cont];
                }

                System.out.println(" Aptidao Pai 1: " + avaliarAptdaoIndividuoVetor(pai1));
                for (int cont = 0; cont < qntGenes; cont++) {
                    if (pai1[cont] == null) {
                        System.out.println(" " + pai1[cont]);
                    } else {
                        if (pai1[cont].getOrientacao() == 0) {
                            o = "VERTICAL";
                        } else {
                            o = "HORIZONTAL";
                        }
                        System.out.println(" X: " + pai1[cont].getX() + " Y: " + pai1[cont].getY()
                                + " ORIENTAÇÃO: " + o);
                    }
                }

                System.out.println("");

                System.out.println(" Aptidao Pai 2: " + avaliarAptdaoIndividuoVetor(pai2));
                for (int cont = 0; cont < qntGenes; cont++) {
                    if (pai2[cont] == null) {
                        System.out.println(" " + pai2[cont]);
                    } else {
                        if (pai2[cont].getOrientacao() == 0) {
                            o = "VERTICAL";
                        } else {
                            o = "HORIZONTAL";
                        }
                        System.out.println(" X: " + pai2[cont].getX() + " Y: " + pai2[cont].getY()
                                + " ORIENTAÇÃO: " + o);
                    }
                }

                System.out.println(" Corte em: " + corte);
                System.out.println("");

                System.out.println(" VERIFICAR SE FILHOS GERADOS SAO VALIDOS: ");
                System.out.println("");

                for (int cont = (corte + 1); cont < qntGenes; cont++) { // Terminar de gerar os filhos
                    filho1[cont] = pai2[cont];
                    filho2[cont] = pai1[cont];
                }

                if (ind < tamPopulacao) {
                    System.out.println(" FILHO 1 ");
                    // FAZER A MUTAÇÃO NO FILHO 1 GERADO
                    mutacaoFilho = radom.nextInt(101); // Gera um numero de 1 a 100
                    if (mutacaoFilho <= mutacao) {
                        System.out.println(" MUTAÇÃO: sim ");
                        mutacaoGene = radom.nextInt(qntGenes);
                        if (filho1[mutacaoGene] == null) {
                            gerarCaixaAleatoria();
                            filho1[mutacaoGene] = caixa;
                        } else {
                            if (filho1[mutacaoGene].getOrientacao() == 0) {
                                filho1[mutacaoGene].setOrientacao(1);
                            } else {
                                filho1[mutacaoGene].setOrientacao(0);
                            }
                        }
                    } else {
                        System.out.println(" MUTAÇÃO: não ");
                    }

                    // VERIFICAR SE O FILHO 1 É VÁLIDO
                    for (int gene = 0; gene < qntGenes; gene++) {
                        if (caixaDentroDoPalete(filho1[gene]) == true) {
                            if (validaIndividuo(filho1, filho1[gene], gene) == false) {
                                filho1 = null;
                                break;
                            }
                        } else {
                            filho1 = null;
                            break;
                        }
                    }
                    if (filho1 == null) {
                        System.out.println(" VÁLIDO: não ");
                    } else {
                        System.out.println(" VÁLIDO: sim ");
                        novaPopulacao[ind] = filho1;
                        ind++;
                    }

                    if (filho1 != null) {  // Mostrar filho 1
                        qtdAptdaoMaxima = avaliarAptdaoIndividuoVetor(filho1);
                        System.out.println(" APTIDÃO: " + avaliarAptdaoIndividuoVetor(filho1));
                        for (int cont = 0; cont < qntGenes; cont++) {
                            if (filho1[cont] == null) {
                                System.out.println(" " + filho1[cont]);
                            } else {
                                if (filho1[cont].getOrientacao() == 0) {
                                    o = "VERTICAL";
                                } else {
                                    o = "HORIZONTAL";
                                }
                                System.out.println(" X: " + filho1[cont].getX() + " Y: " + filho1[cont].getY()
                                        + " ORIENTAÇÃO: " + o);
                            }
                        }
                    }
                }

                System.out.println("");

                if (ind < tamPopulacao) {
                    System.out.println(" FILHO 2 ");
                    // FAZER A MUTAÇÃO NO FILHO 2 GERADO
                    mutacaoFilho = radom.nextInt(101); // Gera um numero de 1 a 100
                    if (mutacaoFilho <= mutacao) {
                        System.out.println(" MUTAÇÃO: sim ");
                        mutacaoGene = radom.nextInt(qntGenes);
                        if (filho2[mutacaoGene] == null) {
                            gerarCaixaAleatoria();
                            filho2[mutacaoGene] = caixa;
                        } else {
                            if (filho2[mutacaoGene].getOrientacao() == 0) {
                                filho2[mutacaoGene].setOrientacao(1);
                            } else {
                                filho2[mutacaoGene].setOrientacao(0);
                            }
                        }
                    } else {
                        System.out.println(" MUTAÇÃO: não ");
                    }

                    // VERIFICAR SE O FILHO 2 É VÁLIDO
                    for (int gene = 0; gene < qntGenes; gene++) {
                        if (caixaDentroDoPalete(filho2[gene]) == true) {
                            if (validaIndividuo(filho2, filho2[gene], gene) == false) {
                                filho2 = null;
                                break;
                            }
                        } else {
                            filho2 = null;
                            break;
                        }
                    }

                    if (filho2 == null) {
                        System.out.println(" VÁLIDO: não ");
                    } else {
                        System.out.println(" VÁLIDO: sim ");
                        novaPopulacao[ind] = filho2;
                        ind++;
                    }

                    if (filho2 != null) {  // Mostrar Filho 2
                        qtdAptdaoMaxima = avaliarAptdaoIndividuoVetor(filho1);
                        System.out.println(" APTIDÃO: " + avaliarAptdaoIndividuoVetor(filho2));
                        for (int cont = 0; cont < qntGenes; cont++) {
                            if (filho2[cont] == null) {
                                System.out.println(" " + filho2[cont]);
                            } else {
                                if (filho2[cont].getOrientacao() == 0) {
                                    o = "VERTICAL";
                                } else {
                                    o = "HORIZONTAL";
                                }
                                System.out.println(" X: " + filho2[cont].getX() + " Y: " + filho2[cont].getY()
                                        + " ORIENTAÇÃO: " + o);
                            }
                        }
                    }
                }
            }

            for (int individuo = 0; individuo < tamPopulacao; individuo++) {
                for (int gene = 0; gene < qntGenes; gene++) {
                    populacao[individuo][gene] = novaPopulacao[individuo][gene];
                }
            }

            novaPopulacao = new Caixa[tamPopulacao][qntGenes];
        }

        System.out.println("");
        mostrarMediaDasAptidoes();
        
        System.out.println("");
        mostrarSolucaoOtimo();
        

        Desenho d;
        d = new Desenho(individuoOtimo, paleteComprimento, paleteLargura, caixaComp, caixaLarg);
    }

    public void mostrarPopulacao() {
        for (int pop = 0; pop < tamPopulacao; pop++) {
            for (int gene = 0; gene < qntGenes; gene++) {
                if (populacao[pop][gene] == null) {
                    System.out.println("null");
                } else {
                    System.out.println("  " + populacao[pop][gene].getX() + "  " + populacao[pop][gene].getY() + "  "
                            + "" + populacao[pop][gene].getOrientacao());
                }
            }
            System.out.println("------------------------");
        }
    }

    public void mostrarSolucaoOtimo() {
        System.out.println(" MELHOR SOLUÇÃO ENCONTRADA ");
        System.out.println(" Geração:" + geracaoOtima);
        for (int gene = 0; gene < qntGenes; gene++) {
            if (individuoOtimo[gene] == null) {
                System.out.println("" + individuoOtimo[gene]);
            } else {
                System.out.println("  " + individuoOtimo[gene].getX() + "  " + individuoOtimo[gene].getY() + "  "
                        + "" + individuoOtimo[gene].getOrientacao());
            }
        }
    }

    public void mostrarMediaDasAptidoes() {
        for (int media = 0; media < ger; media++) {
            System.out.println(" Geração: " + media + " Média: " + mediaAptidoes[media]);
        }
    }
}
